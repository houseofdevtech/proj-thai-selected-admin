<?php
  session_start();
  $script_path = dirname(__FILE__).'/';
  require_once($script_path."api/check_login.php")
?>

<!DOCTYPE html>
<html>
<head>
    <title>เพิ่มรายการร้าน</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../client_script/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href="../client_script/css/mystyle.css?v=8" rel="stylesheet">
    <link href="../client_script/css/animation.css" rel="stylesheet">
    <link href="../client_script/css/navbar.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body onload="resetSelection()">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#">Admin</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2" id="navbarSupportedContent">
            <ul class="nav navbar-nav navbar-right">
                <li style="margin-right: 5vh;" class="nav-item">
                    <a class="nav-link" href="list.php"><span class="glyphicon glyphicon-list"></span> รายการร้าน<span class="sr-only">(current)</span></a>
                  </li>
                  <li style="margin-right: 5vh;" class="nav-item  active">
                    <a class="nav-link" href="add.php"><span class="glyphicon glyphicon-plus-sign"></span> เพิ่มรายการร้าน</a>
                  </li>
                  <li style="margin-right: 5vh;" class="nav-item">
                    <a class="nav-link" href="api/logout.php"><span class="glyphicon glyphicon-log-out"></span> ออกจากระบบ</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class = "col-3"></div>
        <div  class = "fadein col-6" style ="border-style: solid; padding: 5vh; margin: 2vh;">
        <h1 class="text-center title">เพิ่มรายการร้าน</h1>
        <form action="api/add_store.php" method="post" class="justify-content-center">  
            <div class="form-group">
            <label class = "fadein"><h4>ชื่อร้าน</h4></label>
            <label style="color:red">*</label>
            <input class = "fadein form-control" type="text" id="storeName"  name = "storeName" required>
            </div>
            <div class="form-group">
              <label class = "fadein"><h4>ประเภท</h4></label>
              <label style="color:red">*</label>
              <select class = "fadein form-control" id="" name="type">
                  <option value="Thai Select Premium">Thai Select Premium</option>
                  <option value="Thai Select">Thai Select</option>
                  <option value="Thai Select Unique">Thai Select Unique</option>
              </select>
            </div>
            <div class="form-group" accept-charset="utf-8">
              <label class = "fadein"><h4>ที่ตั้ง</h4></label>
              <label style="color:red">*</label>
              <input class = "fadein form-control"  type="text" id=""  name="storeAddress" required>
            </div>
            <div class="form-group">
            <label class = "fadein"><h4>เบอร์โทร</h4></label>
            <label style="color:red">*</label>
            <input class = "fadein form-control" type="tel" id=""  name="storeTel" pattern="[0-9]{9,10}" placeholder="กรุณาใส่ตัวเลขเท่านั้น" required>
            </div>
            <div class="form-group">
            <label class = "fadein"><h4>ภาค</h4></label>
            <label style="color:red">*</label>
            <select class = "fadein form-control" id="category" size="1" onchange="makeSubmenu(this.value)" name="region" required>
              <option value="" disabled selected>เลือกภาค</option>
              <option value="ภาคกลาง">ภาคกลาง</option>
              <option value="ภาคเหนือ">ภาคเหนือ</option>
              <option value="ภาคใต้">ภาคใต้</option>
              <option value="ภาคตะวันออก">ภาคตะวันออก</option>
              <option value="ภาคตะวันตก">ภาคตะวันตก</option>
              <option value="ภาคอีสาน">ภาคอีสาน</option>
            </select>
            </div>
            <div class="form-group">
                <label class = "fadein"><h4>จังหวัด</h4></label>
                <label style="color:red">*</label>
                <select class = "fadein form-control" id="categorySelect" size="1" name="provinceID">
                  <option value="" disabled selected>เลือกจังหวัด</option>
                  <option></option>
                </select>
            </div>
            <div class="form-group">
              <label class = "fadein"><h4>URL</h4></label>
              <label style="color:red">*</label>
              <input class = "fadein form-control" type="text" id=""  name="link">
            </div>
            <div class="form-group">
              <label class = "fadein"><h4>รูปภาพ</h4></label>
              <label style="color:red">*</label>
              <img id="preview">
              <input class = "fadein form-control" type="file" id="upload"  name="upload" required>
              <input type = "hidden" id="Pic" name="Pic" required>
            </div>
            <div  class="text-center"><button type="submit" class="fadein login_button fade-in btn btn-primary btn-lg">ส่ง</button></div>
        </form>
        </div>   
        <div class = "col-3"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script  type="text/javascript" src="../client_script/js/bootstrap/bootstrap.js"></script>
    <script  type="text/javascript" src="../client_script/js/myscript/preview.js"></script>
    <script  type="text/javascript" src="../client_script/js/myscript/prefill.js"></script>
    <script  type="text/javascript" src="../client_script/js/myscript/upload.js?v=2"></script>
    <script  type="text/javascript" src="../client_script/js/myscript/select_province.js?v=2"></script>
</body>
</html>