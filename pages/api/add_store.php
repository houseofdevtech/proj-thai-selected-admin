<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$script_path = dirname(__FILE__).'/';
require_once($script_path.'../../objects/object_path.php');
require_once(STORE);

$store_instance = new Store();

// running number for storecode algorithm
$storeCode = "";
switch ($_POST['region']){
    case "ภาคกลาง":
        $storeCode = "CT";
        
        break;
    case "ภาคเหนือ":
        $storeCode = "NT";
        
        break;
    case "ภาคใต้":
        $storeCode = "ST";
        
        break;
    case "ภาคตะวันออก":
        $storeCode = "ET";
        
        break;
    case "ภาคตะวันตก":
        $storeCode = "WT";
        
        break;
    case "ภาคอีสาน":
        $storeCode = "NE";
        
        break;
}

// check if the query result has double amount of row
$number = count(json_decode($store_instance->get_region($_POST['region']),true));
$storeCode = $storeCode.sprintf("%04d", ($number)+1);

$store_instance->storeName = $_POST['storeName'];
$store_instance->storeAddress = $_POST['storeAddress'];
$store_instance->storeTel = $_POST['storeTel'];
$store_instance->link = $_POST['link'];
$store_instance->Pic = $_POST['Pic'];
$store_instance->storeCode = $storeCode;
$store_instance->provinceID = $_POST['provinceID'];
$store_instance->type = $_POST['type'];
$store_instance->show_data = 1;

var_dump($store_instance->save_to_database());

header('Location: ' . '../list.php');

/*
// file upload and save to /objects/pic
$target_dir = $script_path.'../../objects/pic/';
$name_array = explode(".",basename($_FILES["fileToUpload"]["name"]));
$rename_target_file = $name_array[0]."_".$storeCode.".".$name_array[count($name_array)-1];
$target_file = $target_dir . $rename_target_file;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

//var_dump($imageFileType);

$uploadOk = 1;

if (file_exists($target_file)) {
    echo "Sorry, file already exists. Please rename your file before uploading";
    $uploadOk = 0;
  }
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
  }
  
  // Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
  // if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
      //store to database
      $store_instance->storeName = $_POST['storeName'];
      $store_instance->storeAddress = $_POST['storeAddress'];
      $store_instance->storeTel = $_POST['storeTel'];
      $store_instance->link = $_POST['link'];
      $store_instance->Pic = $target_file;
      $store_instance->storeCode = $storeCode;
      $store_instance->provinceID = $_POST['provinceID'];
      $store_instance->type = $_POST['type'];

      var_dump($store_instance->save_to_database());
    } else {
      echo "Sorry, there was an error uploading your file.";
    }
  }
*/












?>