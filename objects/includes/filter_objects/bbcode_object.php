<?php
/**
*	Common BBCode Filter Handling Object
* 	Created 14-7-2012
* */	
	/*Internal Lib Require*/
	require_once(dirname(__FILE__).'/'.'text_object.php');
		
	class bbcode_object extends text_object {
		
		function bbcode_object($text, $charset='utf-8') {
			parent::text_object($text, $charset);
		}
		
		function remove_unwanted_text(){
			$pattern = '/^[^a-zA-Z0-9ก-๙]+|[^a-zA-Z0-9ก-๙\s\.?!,\/%]/u';
			$this->_data_processing_text = preg_replace($pattern, "", $this->_data_processing_text);
			return $this->_data_processing_text;
		}

		function clean_html_special_character(){
			$this->_data_processing_text = trim(preg_replace('/&\w*;/', '', $this->_data_processing_text));
			$this->_data_processing_text = trim(preg_replace('/\n/i', '', $this->_data_processing_text));
			return $this->_data_processing_text;
		}

		function clean_quote() {
			$this->_data_processing_text = trim(preg_replace('/\[QUOTE(.)+?\]?.+\[\/QUOTE\]/i', '', $this->_data_processing_text));
			return $this->_data_processing_text;
		}

		function clearn_img(){
			$this->_data_processing_text = trim(preg_replace('/\[IMG(.)+?\]?.+\[\/IMG\]/i', '', $this->_data_processing_text));
			return $this->_data_processing_text;
		}
		
		function clean_tag() {
			$this->_data_processing_text = trim(preg_replace('/\[(.)+?\]/i', '', $this->_data_processing_text));
			return $this->_data_processing_text;
		}
		
		function get_intro_text($length='100', $replacer='...') {
			$this->clean_quote();
			$this->clearn_img();
			$this->clean_tag();
			$this->clean_html_special_character();
			$this->remove_unwanted_text();
			return $this->limit_text($length, $replacer);
		}
		
		function get_image_tags() {
			preg_match_all('/(\[img[\s.]*\]https*.+?\[\/img\])/i', $this->_data_processing_text, $image_tags);
			return $image_tags[0];
		}
		
		function get_image_urls($exception_extensions = '') {
			$image_tags = $this->get_image_tags();

			$image_urls = array();
			if ((is_array($image_tags) == FALSE) && (count($image_tags) == 0)) {
				return $image_urls;
			}
			
			if (is_array($exception_extensions) == TRUE) {
				/*convert all elements to lowercase*/
				$exception_extensions = array_map('strtolower', $exception_extensions);
			}
			
			foreach ($image_tags as $index => $tags) {
				$tmp_img_src = trim(preg_replace('/(\[\/*.+?])/i', '', $tags));
				
				if (is_array($exception_extensions) == FALSE) {
					$image_urls[$index] = $tmp_img_src;
				} else {
					$current_extension = strtolower(pathinfo($tmp_img_src, PATHINFO_EXTENSION));
					if (array_search($current_extension, $exception_extensions) === FALSE) {
						$image_urls[$index] = $tmp_img_src;
					}
				}
			}
			
			return $image_urls;
		}

		/*modified code from //http://kuikie.com/snippet/90-32/php/strings/php-function-to-convert-bbcode-to-html/%7B$ROOT_URL%7D/terms/*/
		function bbcode_to_html(){

			$bbtext = $this->_data_processing_text;

		  	$bbtags = array(
			    '[heading1]' => '<h1>','[/heading1]' => '</h1>',
			    '[heading2]' => '<h2>','[/heading2]' => '</h2>',
			    '[heading3]' => '<h3>','[/heading3]' => '</h3>',
			    '[h1]' => '<h1>','[/h1]' => '</h1>',
			    '[h2]' => '<h2>','[/h2]' => '</h2>',
			    '[h3]' => '<h3>','[/h3]' => '</h3>',

			    '[paragraph]' => '<p>','[/paragraph]' => '</p>',
			    '[para]' => '<p>','[/para]' => '</p>',
			    '[p]' => '<p>','[/p]' => '</p>',
			    '[left]' => '<p style="text-align:left;">','[/left]' => '</p>',
			    '[right]' => '<p style="text-align:right;">','[/right]' => '</p>',
			    '[center]' => '<p style="text-align:center;">','[/center]' => '</p>',
			    '[justify]' => '<p style="text-align:justify;">','[/justify]' => '</p>',

			    '[bold]' => '<span style="font-weight:bold;">','[/bold]' => '</span>',
			    '[italic]' => '<span style="font-weight:bold;">','[/italic]' => '</span>',
			    '[underline]' => '<span style="text-decoration:underline;">','[/underline]' => '</span>',
			    '[b]' => '<span style="font-weight:bold;">','[/b]' => '</span>',
			    '[i]' => '<span style="font-weight:bold;">','[/i]' => '</span>',
			    '[u]' => '<span style="text-decoration:underline;">','[/u]' => '</span>',
			    '[break]' => '<br>',
			    '[br]' => '<br>',
			    '[newline]' => '<br>',
			    '[nl]' => '<br>',
			    
			    '[unordered_list]' => '<ul>','[/unordered_list]' => '</ul>',
			    '[list]' => '<ul>','[/list]' => '</ul>',
			    '[ul]' => '<ul>','[/ul]' => '</ul>',

			    '[ordered_list]' => '<ol>','[/ordered_list]' => '</ol>',
			    '[ol]' => '<ol>','[/ol]' => '</ol>',
			    '[list_item]' => '<li>','[/list_item]' => '</li>',
			    '[li]' => '<li>','[/li]' => '</li>',
			    
			    '[*]' => '<li>','[/*]' => '</li>',
			    '[code]' => '<code>','[/code]' => '</code>',
			    '[preformatted]' => '<pre>','[/preformatted]' => '</pre>',
			    '[pre]' => '<pre>','[/pre]' => '</pre>',	    

			    '[size=1]' => '<span class="bb_text_size1">', 
			    '[size=2]' => '<span class="bb_text_size2">',
			    '[size=3]' => '<span class="bb_text_size3">',
			    '[size=4]' => '<span class="bb_text_size4">',
			    '[size=5]' => '<span class="bb_text_size5">',
			    '[size=6]' => '<span class="bb_text_size6">',
			    '[/size]' => '</span>',
			    '[/color]' => '</span>',
			    '[/font]'  => '</span>'

		  	);

			$bbtext = str_ireplace(array_keys($bbtags), array_values($bbtags), $bbtext);

			$bbextended = array(
			 	'/\n/' => "<br>",
			    "/\[url](.*?)\[\/url]/si" => "<a class='bb_url' ref='nofollow' target='_blank' href=\"$1\" title=$1>$1</a>",
			    "/\[url=(.*?)\](.*?)\[\/url\]/i" => "<a class='bb_url' ref='nofollow' target='_blank' href=$1 title=$1>$2</a>",
			    "/\[email=(.*?)\](.*?)\[\/email\]/i" => "<a class='bb_url' ref='nofollow' target='_blank' href=\"mailto:$1\">$2</a>",
			    "/\[mail=(.*?)\](.*?)\[\/mail\]/i" => "<a class='bb_url' ref='nofollow' target='_blank' href=\"mailto:$1\">$2</a>",
			    "/\[img\]([^[]*)\[\/img\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" />",
			    "/\[image\]([^[]*)\[\/image\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" />",
			    "/\[image_left\]([^[]*)\[\/image_left\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" class=\"img_left\" />",
			    "/\[image_right\]([^[]*)\[\/image_right\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" class=\"img_right\" />",
			    "/\[color=\"(.*?)\"\]/i" => "<span style='color:$1'>",
			    "/\[color=\'(.*?)\'\]/i" => "<span style='color:$1'>", 
			    "/\[color=(.*?)\]/i" => "<span style='color:$1'>",
			    "/\[font=(.*?)\]/i" => "<span style='font-family:$1'>",
			    "/\[quote=(.*?);(.*?)\](.*?)\[\/quote\]/i" => "<div class='bb_quote'><div class='bb_quote_arrow'></div><div class='bb_quote_arrow_border'></div>Originally Posted by $1<br>$3</div>",
			    "/\[video=youtube;(.*?)\](.*?)\[\/video\]/si" => "<iframe src='http://www.youtube.com/embed/$1?autoplay=0' frameborder='0'></iframe>"
			);

			foreach($bbextended as $match=>$replacement){
		    	$bbtext = preg_replace($match, $replacement, $bbtext);
			}

			$this->_data_processing_text = $bbtext;

			$this->clean_quote();
			$this->clean_tag();

			return $this->_data_processing_text;
		}

		/*modified code from //http://kuikie.com/snippet/90-32/php/strings/php-function-to-convert-bbcode-to-html/%7B$ROOT_URL%7D/terms/*/
		function bbcode_to_html_2(){

			$bbtext = $this->_data_processing_text;

		  	$bbtags = array(
			    '[heading1]' => '<h1>','[/heading1]' => '</h1>',
			    '[heading2]' => '<h2>','[/heading2]' => '</h2>',
			    '[heading3]' => '<h3>','[/heading3]' => '</h3>',
			    '[h1]' => '<h1>','[/h1]' => '</h1>',
			    '[h2]' => '<h2>','[/h2]' => '</h2>',
			    '[h3]' => '<h3>','[/h3]' => '</h3>',

			    '[paragraph]' => '<p>','[/paragraph]' => '</p>',
			    '[para]' => '<p>','[/para]' => '</p>',
			    '[p]' => '<p>','[/p]' => '</p>',
			    '[left]' => '<p style="text-align:left;">','[/left]' => '</p>',
			    '[right]' => '<p style="text-align:right;">','[/right]' => '</p>',
			    '[center]' => '<p style="text-align:center;">','[/center]' => '</p>',
			    '[justify]' => '<p style="text-align:justify;">','[/justify]' => '</p>',

			    '[bold]' => '<span style="font-weight:bold;">','[/bold]' => '</span>',
			    '[italic]' => '<span style="font-weight:bold;">','[/italic]' => '</span>',
			    '[underline]' => '<span style="text-decoration:underline;">','[/underline]' => '</span>',
			    '[b]' => '<span style="font-weight:bold;">','[/b]' => '</span>',
			    '[i]' => '<span style="font-weight:bold;">','[/i]' => '</span>',
			    '[u]' => '<span style="text-decoration:underline;">','[/u]' => '</span>',
			    '[break]' => '<br>',
			    '[br]' => '<br>',
			    '[newline]' => '<br>',
			    '[nl]' => '<br>',
			    
			    '[unordered_list]' => '<ul>','[/unordered_list]' => '</ul>',
			    '[list]' => '<ul>','[/list]' => '</ul>',
			    '[ul]' => '<ul>','[/ul]' => '</ul>',

			    '[ordered_list]' => '<ol>','[/ordered_list]' => '</ol>',
			    '[ol]' => '<ol>','[/ol]' => '</ol>',
			    '[list_item]' => '<li>','[/list_item]' => '</li>',
			    '[li]' => '<li>','[/li]' => '</li>',
			    
			    '[*]' => '<li>','[/*]' => '</li>',
			    '[code]' => '<code>','[/code]' => '</code>',
			    '[preformatted]' => '<pre>','[/preformatted]' => '</pre>',
			    '[pre]' => '<pre>','[/pre]' => '</pre>',	    

			    '[size=1]' => '<span class="bb_text_size1">', 
			    '[size=2]' => '<span class="bb_text_size2">',
			    '[size=3]' => '<span class="bb_text_size3">',
			    '[size=4]' => '<span class="bb_text_size4">',
			    '[size=5]' => '<span class="bb_text_size5">',
			    '[size=6]' => '<span class="bb_text_size6">',
			    '[/size]' => '</span>',
			    '[/color]' => '</span>',
			    '[/font]'  => '</span>'

		  	);

			$bbtext = str_ireplace(array_keys($bbtags), array_values($bbtags), $bbtext);

			$bbextended = array(
			 	'/\n/' => "<br>",
			    "/\[url](.*?)\[\/url]/si" => "<a class='bb_url' ref='nofollow' target='_blank' href=\"$1\" title=$1>$1</a>",
			    "/\[url=(.*?)\](.*?)\[\/url\]/i" => "<a class='bb_url' ref='nofollow' target='_blank' href=$1 title=$1>$2</a>",
			    "/\[email=(.*?)\](.*?)\[\/email\]/i" => "<a class='bb_url' ref='nofollow' target='_blank' href=\"mailto:$1\">$2</a>",
			    "/\[mail=(.*?)\](.*?)\[\/mail\]/i" => "<a class='bb_url' ref='nofollow' target='_blank' href=\"mailto:$1\">$2</a>",
			    "/\[img\]([^[]*)\[\/img\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" />",
			    "/\[image\]([^[]*)\[\/image\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" />",
			    "/\[image_left\]([^[]*)\[\/image_left\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" class=\"img_left\" />",
			    "/\[image_right\]([^[]*)\[\/image_right\]/i" => "<img class='bb_img' src=\"$1\" alt=\" \" class=\"img_right\" />",
			    "/\[color=\"(.*?)\"\]/i" => "<span style='color:$1'>",
			    "/\[color=\'(.*?)\'\]/i" => "<span style='color:$1'>", 
			    "/\[color=(.*?)\]/i" => "<span style='color:$1'>",
			    "/\[font=(.*?)\]/i" => "<span style='font-family:$1'>",
			    "/\[quote](.*?)\[\/quote]/si" => "<div class='bb_quote'><div class='bb_quote_arrow'></div><div class='bb_quote_arrow_border'></div>$1</div>",
			    "/\[quote=(.*?);(.*?)\](.*?)\[\/quote\]/i" => "<div class='bb_quote'><div class='bb_quote_arrow'></div><div class='bb_quote_arrow_border'></div>Originally Posted by $1<br>$3</div>",
			    "/\[video=youtube;(.*?)\](.*?)\[\/video\]/si" => "<iframe src='http://www.youtube.com/embed/$1?autoplay=0' frameborder='0'></iframe>"
			);

			foreach($bbextended as $match=>$replacement){
		    	$bbtext = preg_replace($match, $replacement, $bbtext);
			}

			$this->_data_processing_text = $bbtext;

			$this->clean_quote();
			//$this->clean_tag();

			return $this->_data_processing_text;
		}

	}
?>