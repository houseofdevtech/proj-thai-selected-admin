<?php

	$script_path = dirname(__FILE__).'/';

	require_once($script_path.'../object_path.php');
	require_once(RSF_DATABASE_OBJECTS_PATH_WITHOUT_MEMCACHE);

	class final_image extends database_object {


		var
			$final_image_id, 
			$image_data,
			$template_id,
			$is_confirmed,
			$is_exported,
			$gallery_id,
			$image_data_type;

		private 
			$table_name;


		function final_image($table_name = 'final_image'){

			$this->table_name = $table_name;
			parent::database_object($table_name);
		}

		function get_final_image_list(){

			$sql = "
		        SELECT final_image_id, image_data   
		        FROM $this->table_name 
		        ORDER BY final_image_id DESC
		    ";
		    $db_instance = database_object::connect_database($db_instance);
		    
		    return $db_instance->GetAll($sql); 
		}


	}

?>