<?php

	/*php mailer*/
	DEFINE('PHP_MAILER', dirname(__FILE__)."/".'external_lib_core/php_mailer/PHPMailerAutoload.php');

	/*Smarty Library Definition*/
	DEFINE('SMARTY_PATH', dirname(__FILE__)."/".'../../SBNFunction/smarty/Smarty.class.php');//smarty version 2
	DEFINE('SMARTY_PATH3', dirname(__FILE__)."/".'external_lib_core/Smarty-3.1.27/Smarty.class.php');//smarty version 3

	/*Mysql Connection Library Definition*/
	DEFINE('MYSQL_CONNECT_PATH',dirname(__FILE__)."/".'../../SBNFunction/ConnectMySQL.php');
	
	/*RSF Database Munipulation Object Library Definition*/
	DEFINE('RSF_DATABASE_OBJECTS_PATH', dirname(__FILE__)."/".'database_objects/database_object.php');

	/**RSF Database Munipulation Object Library Definition without memcache*/
	DEFINE('RSF_DATABASE_OBJECTS_PATH_WITHOUT_MEMCACHE', dirname(__FILE__)."/".'database_objects/database_object_without_memcache.php');

	/*RSF Database Munipulation Object Library Definition*/
	DEFINE('RSF_MEMCACHED_OBJECTS_PATH', dirname(__FILE__)."/".'cached_objects/memcached_object.php');
		
	/*Standard RSF Object Library Definition*/
	DEFINE('RSF_STANDARD_OBJECTS_PATH', dirname(__FILE__)."/".'standard_objects/standard_object.php');
	
	/*RSF BBCode Processing Object Library Definition*/
	DEFINE('RSF_BBCODE_OBJECT_PATH', dirname(__FILE__)."/".'filter_objects/bbcode_object.php');	 
	
	/*RSF Text Processing Object Library Definition*/
	DEFINE('RSF_TEXT_OBJECT_PATH', dirname(__FILE__)."/".'filter_objects/text_object.php');	

	/*PImage -- Image Tools Libraryy Definition*/
	DEFINE('P_IMAGES_PATH', dirname(__FILE__)."/".'../../SBNFunction/PImages.php');
	
	/*PFtp -- FTP Connection Library Definition*/
	DEFINE('P_FTP_PATH', dirname(__FILE__)."/".'../../SBNFunction/PFtp.php');

	/*PFiles -- File Management Library Definition*/
	DEFINE('P_FILES_PATH', dirname(__FILE__)."/".'../../SBNFunction/PFiles.php');	

	/*easy_CURL Library Definition*/
	DEFINE('EASY_CURL', dirname(__FILE__)."/".'external_lib_core/easy_curl/easy_curl.php');

	/*biff_XLS Library Definition*/
	DEFINE('BIFF_XLS', dirname(__FILE__)."/".'external_lib_core/biff_xls/biff_xls.php');
?>