<?php

	$script_path = dirname(__FILE__).'/';

	require_once($script_path.'../object_path.php');
	require_once(RSF_DATABASE_OBJECTS_PATH_WITHOUT_MEMCACHE);

	class Store extends database_object {


		var
			$storeID,
			$storeName,
			$storeAddress,
			$storeTel,
			$link,
			$Pic,
			$storeCode,
			$provinceID,
			$type,
			$show_data
			//$date_created,
			//$date_update
		;

		private
			$table_name;


		function __construct($table_name = 'store'){

			$this->table_name = $table_name;
			parent::database_object($table_name);

		}

		


		function get_Alllist(){

			$sql = "
				SELECT *
				FROM store
				LEFT JOIN province 
				on store.provinceID = province.provinceID
				WHERE show_data=1
			";

			$db_instance = database_object::connect_database($db_instance);
		    $result = $db_instance->GetAll($sql);

		    return json_encode($result);
		}

		function get_10list($id){

			$sql = "
				SELECT *
				FROM store
				WHERE show_data=1
				AND provinceID=".$id."
				LIMIT 10
				";
			
				$db_instance = database_object::connect_database($db_instance);
		    $result = $db_instance->GetAll($sql);

		    return json_encode($result);
			}

		function get_by_province($provinceID){
			$sql = "
				SELECT * 
				FROM store  
				WHERE provinceID = ".$provinceID."
			";
			$db_instance = database_object::connect_database($db_instance);
			$result = $db_instance->GetAll($sql); 

			return json_encode($result);
		}

		function get_region($region){

            $sql = "
				SELECT * 
				FROM store 
				LEFT JOIN province 
				on store.provinceID = province.provinceID 
				WHERE province.region = '".$region."'
            ";

            $db_instance = database_object::connect_database($db_instance);
            $result = $db_instance->GetAll($sql); 

            return json_encode($result);
		}
		
		function list_by_region($region){

            $sql = "
				SELECT * 
				FROM store 
				LEFT JOIN province 
				on store.provinceID = province.provinceID 
				WHERE province.region = '".$region."'
				AND show_data=1
            ";

            $db_instance = database_object::connect_database($db_instance);
            $result = $db_instance->GetAll($sql); 

            return json_encode($result);
		}

		function get_by_id($id){
			$sql = "
				SELECT * 
				FROM store  
				WHERE storeID = ".$id."
			";
			$db_instance = database_object::connect_database($db_instance);
            $result = $db_instance->GetAll($sql); 

            return json_encode($result);
		}

		function get_province_by_region($region){
			$sql = "
				SELECT * 
				FROM province  
				WHERE region = '".$region."'
			";
			$db_instance = database_object::connect_database($db_instance);
            $result = $db_instance->GetAll($sql); 

            return json_encode($result);
		}

		function get_province(){
			$sql = "
				SELECT * 
				FROM province 
			";
			$db_instance = database_object::connect_database($db_instance);
            $result = $db_instance->GetAll($sql); 

            return json_encode($result);
		}

		function toggle_show_data($id){
			$sql = "
				UPDATE store
				SET show_data=0
				WHERE storeID=".$id."
			";
			$db_instance = database_object::connect_database($db_instance);
            $result = $db_instance->Execute($sql); 

            return json_encode($result);
		}
//use when we have new image incoming
		function edit($storeName,
		$storeAddress,
		$storeTel,
		$link,
		$Pic,
		$provinceID,
		$type,
		$id){
			$sql = "
				UPDATE store
				SET storeName='".$storeName."',
				storeAddress='".$storeAddress."',
				storeTel='".$storeTel."',
				link='".$link."',
				Pic='".$Pic."',
				provinceID=".$provinceID.",
				type='".$type."'
				WHERE storeID=".$id."
			";
			$db_instance = database_object::connect_database($db_instance);
            $result = $db_instance->Execute($sql); 

            return json_encode($result);
		}

//use when we DON'T have new image incoming
		function edit_nopic($storeName,
		$storeAddress,
		$storeTel,
		$link,
		$provinceID,
		$type,
		$id){
			$sql = "
				UPDATE store
				SET storeName='".$storeName."',
				storeAddress='".$storeAddress."',
				storeTel='".$storeTel."',
				link='".$link."',
				provinceID=".$provinceID.",
				type='".$type."'
				WHERE id=".$id."
			";
			$db_instance = database_object::connect_database($db_instance);
			$result = $db_instance->Execute($sql); 

			return json_encode($result);
		}
/*
		function get_scan_time($guest_id, $day, $month, $year){
			$sql = "
		        SELECT create_timestamp
				FROM ".$this->table_name."
				WHERE guest_id = ".$guest_id." &&
				day = ".$day." &&
				month = ".$month." &&
				year = ".$year."
		    ";

		    $db_instance = database_object::connect_database($db_instance);
		    $result = $db_instance->GetAll($sql);
		    if( $result != null){
		    	return $result[0][0];
			}
			else{
				return "";
			}
		}
*/

	}

?>
