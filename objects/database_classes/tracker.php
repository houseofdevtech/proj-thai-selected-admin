<?php

	$script_path = dirname(__FILE__).'/';

	require_once($script_path.'../object_path.php');
	require_once(RSF_DATABASE_OBJECTS_PATH_WITHOUT_MEMCACHE);

	class tracker extends database_object {


		var
			$Id,
			$Distance,
			$Date,
			$Start,
			$Finish
			
		;

		private 
			$table_name;


		function __construct($table_name = 'Tracker'){

			$this->table_name = $table_name;
			parent::database_object($table_name);

		}


		function get_tracker_list(){

			$sql = "
				SELECT * 
				FROM Tracker
			";

			$db_instance = database_object::connect_database($db_instance);
		    $result = $db_instance->GetAll($sql); 

		    return json_encode($result);
		}



/*
		function get_scan_time($guest_id, $day, $month, $year){
			$sql = "
		        SELECT create_timestamp  
				FROM ".$this->table_name." 
				WHERE guest_id = ".$guest_id." && 
				day = ".$day." &&  
				month = ".$month." && 
				year = ".$year." 
		    ";

		    $db_instance = database_object::connect_database($db_instance);
		    $result = $db_instance->GetAll($sql); 
		    if( $result != null){
		    	return $result[0][0];
			}
			else{
				return "";
			}
		} 
*/

	}

?>