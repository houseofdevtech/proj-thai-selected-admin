var subcategory =  {
	ภาคกลาง: [],
	ภาคตะวันตก: [],
	ภาคตะวันออก: [],
	ภาคอีสาน: [],
	ภาคเหนือ: [],
	ภาคใต้: []
};

$(document).ready(function(){
	var settings = {
		"url": "api/listAllProvince.php",
		"method": "GET",
		"timeout": 0,
	};
    
	
	$.ajax(settings).done(function (response) {

		var obj = jQuery.parseJSON(response);
		// console.log("response");
		// console.log(response);
			console.log("obj");
			console.log(obj);
			$.each(obj, function(key, value){
			subcategory[value.region].push(value.provinceName+value.provinceID);
			});
	});

	console.log(subcategory);


});

function makeSubmenu(value) {
	if (value.length == 0) document.getElementById("categorySelect").innerHTML = "<option></option>";
	else {
		var province = "";
		for (categoryId in subcategory[value]) {
			var reg1 = /^.+\D/;
			var provinceName = subcategory[value][categoryId].match(reg1);
			var reg2 = /\d{1,2}/;
			var provinceID = subcategory[value][categoryId].match(reg2);
			province += "<option value = '"+ provinceID +"'>" + provinceName + "</option>";
		}
		document.getElementById("categorySelect").innerHTML = province;
	}
}

function resetSelection() {
	document.getElementById("category").selectedIndex = 0;
	document.getElementById("categorySelect").selectedIndex = 0;
}
