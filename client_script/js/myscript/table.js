$(document).ready(function(){

	var settings = {
	  "url": "api/selectall.php",
	  "method": "GET",
	  "timeout": 0,
	};


	$.ajax(settings).done(function (response) {

		var obj = jQuery.parseJSON(response);
		// console.log("response");
		// console.log(response);
	  	console.log("obj");
	  	console.log(obj);

	  	$.each(obj, function(key, value){

			$('.list').append('<tr>'+
				'<td class = "fadein">'+value.storeCode+'</td>'+
				'<td class = "fadein">'+value.storeName+'</td>'+
				'<td class = "fadein">'+value.type+'</td>'+
				'<td class = "fadein">'+value.storeTel+'</td>'+
				'<td class = "fadein">'+value.region+'</td>'+
				'<td class = "fadein">'+value.provinceName+'</td>'+
				'<td class = "fadein">'+value.storeAddress+'</td>'+
				'<td class = "fadein">'+value.link+'</td>'+
				'<td class = "fadein"><img class = "table_pic" src = "'+value.Pic+'"></td>'+
				'<td class = "fadein"><p><a  href= "edit.php?id="'+value.storeID+'><span class="glyphicon glyphicon-edit "></span>'+"แก้ไข".link("edit.php?id="+value.storeID)+'</a></p></td>'+
				'<td class = "fadein"><p class="delete"><a class="confirm delete" store_ID = "' +value.storeID+  '" store_Name = "'+value.storeName +'" ><span class="delete glyphicon glyphicon-remove-sign "></span>'+"ลบ"+'</a></p></td>'
				+'</tr>');
		});
	});

	var settings1 = {
		"url": 'api/selectByRegion.php?region=ภาคกลาง',
		"method": "GET",
		"timeout": 0,
	  };
  
  
	  $.ajax(settings1).done(function (response) {
  
		  var obj = jQuery.parseJSON(response);
		  // console.log("response");
		  // console.log(response);
			console.log("obj");
			console.log(obj);
  
			$.each(obj, function(key, value){
			  $('.center').append('<tr>'+
				  '<td class = "fadein">'+value.storeCode+'</td>'+
				  '<td class = "fadein">'+value.storeName+'</td>'+
				  '<td class = "fadein">'+value.type+'</td>'+
				  '<td class = "fadein">'+value.storeTel+'</td>'+
				  '<td class = "fadein">'+value.region+'</td>'+
				  '<td class = "fadein">'+value.provinceName+'</td>'+
				  '<td class = "fadein">'+value.storeAddress+'</td>'+
				  '<td class = "fadein">'+value.link+'</td>'+
				  '<td class = "fadein"><img  class = "table_pic" src = "'+value.Pic+'"></td>'+
				  '<td class = "fadein"><p><a  href= "edit.php?id="'+value.storeID+'><span class="glyphicon glyphicon-edit "></span>'+"แก้ไข".link("edit.php?id="+value.storeID)+'</a></p></td>'+
				'<td class = "fadein"><p class="delete"><a class="confirm delete" store_ID = "' +value.storeID+  '" store_Name = "'+value.storeName +'"><span class="delete glyphicon glyphicon-remove-sign "></span>'+"ลบ"+'</a></p></td>'
				  +'</tr>');
		  });
  
	  });
  
	  var settings2 = {
		  "url": "api/selectByRegion.php?region=ภาคเหนือ",
		  "method": "GET",
		  "timeout": 0,
		};
  
	  $.ajax(settings2).done(function (response) {
  
		  var obj = jQuery.parseJSON(response);
		  // console.log("response");
		  // console.log(response);
			console.log("obj");
			console.log(obj);
  
			$.each(obj, function(key, value){
			  $('.north').append('<tr>'+
				'<td class = "fadein">'+value.storeCode+'</td>'+
				'<td class = "fadein">'+value.storeName+'</td>'+
				'<td class = "fadein">'+value.type+'</td>'+
				'<td class = "fadein">'+value.storeTel+'</td>'+
				'<td class = "fadein">'+value.region+'</td>'+
				'<td class = "fadein">'+value.provinceName+'</td>'+
				'<td class = "fadein">'+value.storeAddress+'</td>'+
				'<td class = "fadein">'+value.link+'</td>'+
				'<td class = "fadein"><img class = "table_pic" src = "'+value.Pic+'"></td>'+
				'<td class = "fadein"><p><a  href= "edit.php?id="'+value.storeID+'><span class="glyphicon glyphicon-edit "></span>'+"แก้ไข".link("edit.php?id="+value.storeID)+'</a></p></td>'+
				'<td class = "fadein"><p class="delete"><a class="confirm delete" store_ID = "' +value.storeID+  '" store_Name = "'+value.storeName +'"><span class="delete glyphicon glyphicon-remove-sign "></span>'+"ลบ"+'</a></p></td>'
				+'</tr>');

		  });
  
	  });
  
	  var settings3 = {
		  "url": "api/selectByRegion.php?region=ภาคอีสาน",
		  "method": "GET",
		  "timeout": 0,
		};
  
	  $.ajax(settings3).done(function (response) {
  
		  var obj = jQuery.parseJSON(response);
		  // console.log("response");
		  // console.log(response);
			console.log("obj");
			console.log(obj);
  
			$.each(obj, function(key, value){
			  $('.north_east').append('<tr>'+
				'<td class = "fadein">'+value.storeCode+'</td>'+
				'<td class = "fadein">'+value.storeName+'</td>'+
				'<td class = "fadein">'+value.type+'</td>'+
				'<td class = "fadein">'+value.storeTel+'</td>'+
				'<td class = "fadein">'+value.region+'</td>'+
				'<td class = "fadein">'+value.provinceName+'</td>'+
				'<td class = "fadein">'+value.storeAddress+'</td>'+
				'<td class = "fadein">'+value.link+'</td>'+
				'<td class = "fadein"><img class = "table_pic" src = "'+value.Pic+'"></td>'+
				'<td class = "fadein"><p><a  href= "edit.php?id="'+value.storeID+'><span class="glyphicon glyphicon-edit "></span>'+"แก้ไข".link("edit.php?id="+value.storeID)+'</a></p></td>'+
				'<td class = "fadein"><p class="delete"><a class="confirm delete" store_ID = "' +value.storeID+  '" store_Name = "'+value.storeName +'"><span class="delete glyphicon glyphicon-remove-sign "></span>'+"ลบ"+'</a></p></td>'
				+'</tr>');

		  });
  
	  });
  
	  var settings4 = {
		  "url": "api/selectByRegion.php?region=ภาคตะวันออก",
		  "method": "GET",
		  "timeout": 0,
		};
  
	  $.ajax(settings4).done(function (response) {
  
		  var obj = jQuery.parseJSON(response);
		  // console.log("response");
		  // console.log(response);
			console.log("obj");
			console.log(obj);
  
			$.each(obj, function(key, value){
			  $('.east').append('<tr>'+
				'<td class = "fadein">'+value.storeCode+'</td>'+
				'<td class = "fadein">'+value.storeName+'</td>'+
				'<td class = "fadein">'+value.type+'</td>'+
				'<td class = "fadein">'+value.storeTel+'</td>'+
				'<td class = "fadein">'+value.region+'</td>'+
				'<td class = "fadein">'+value.provinceName+'</td>'+
				'<td class = "fadein">'+value.storeAddress+'</td>'+
				'<td class = "fadein">'+value.link+'</td>'+
				'<td class = "fadein"><img class = "table_pic" src = "'+value.Pic+'"></td>'+
				'<td class = "fadein"><p><a  href= "edit.php?id="'+value.storeID+'><span class="glyphicon glyphicon-edit "></span>'+"แก้ไข".link("edit.php?id="+value.storeID)+'</a></p></td>'+
				'<td class = "fadein"><p class="delete"><a class="confirm delete" store_ID = "' +value.storeID+  '" store_Name = "'+value.storeName +'"><span class="delete glyphicon glyphicon-remove-sign "></span>'+"ลบ"+'</a></p></td>'
				+'</tr>');
		  });
  
	  });
  
	  var settings5 = {
		  "url": "api/selectByRegion.php?region=ภาคตะวันตก",
		  "method": "GET",
		  "timeout": 0,
		};
  
	  $.ajax(settings5).done(function (response) {
  
		  var obj = jQuery.parseJSON(response);
		  // console.log("response");
		  // console.log(response);
			console.log("obj");
			console.log(obj);
  
			$.each(obj, function(key, value){
			  $('.west').append('<tr>'+
			'<td class = "fadein">'+value.storeCode+'</td>'+
			'<td class = "fadein">'+value.storeName+'</td>'+
			'<td class = "fadein">'+value.type+'</td>'+
			'<td class = "fadein">'+value.storeTel+'</td>'+
			'<td class = "fadein">'+value.region+'</td>'+
			'<td class = "fadein">'+value.provinceName+'</td>'+
			'<td class = "fadein">'+value.storeAddress+'</td>'+
			'<td class = "fadein">'+value.link+'</td>'+
			'<td class = "fadein"><img class = "table_pic" class = "table_pic" src = "'+value.Pic+'"></td>'+
			'<td class = "fadein"><p><a  href= "edit.php?id="'+value.storeID+'><span class="glyphicon glyphicon-edit "></span>'+"แก้ไข".link("edit.php?id="+value.storeID)+'</a></p></td>'+
				'<td class = "fadein"><p class="delete"><a class="confirm delete" store_ID = "' +value.storeID+  '" store_Name = "'+value.storeName +'"><span class="delete glyphicon glyphicon-remove-sign "></span>'+"ลบ"+'</a></p></td>'
			+'</tr>');
		  });
  
	  });
  
	  var settings6 = {
		  "url": "api/selectByRegion.php?region=ภาคใต้",
		  "method": "GET",
		  "timeout": 0,
		};
  
	  $.ajax(settings6).done(function (response) {
  
		  var obj = jQuery.parseJSON(response);
		  // console.log("response");
		  // console.log(response);
			console.log("obj");
			console.log(obj);
  
			$.each(obj, function(key, value){
			  $('.south').append('<tr>'+
				'<td class = "fadein">'+value.storeCode+'</td>'+
				'<td class = "fadein">'+value.storeName+'</td>'+
				'<td class = "fadein">'+value.type+'</td>'+
				'<td class = "fadein">'+value.storeTel+'</td>'+
				'<td class = "fadein">'+value.region+'</td>'+
				'<td class = "fadein">'+value.provinceName+'</td>'+
				'<td class = "fadein">'+value.storeAddress+'</td>'+
				'<td class = "fadein">'+value.link+'</td>'+
				'<td class = "fadein"><img  class = "table_pic" src = "'+value.Pic+'"></td>'+
				'<td class = "fadein"><p><a  href= "edit.php?id="'+value.storeID+'><span class="glyphicon glyphicon-edit "></span>'+"แก้ไข".link("edit.php?id="+value.storeID)+'</a></p></td>'+
				'<td class = "fadein"><p class="delete"><a class="confirm delete" store_ID = "' +value.storeID+  '" store_Name = "'+value.storeName +'"><span class="delete glyphicon glyphicon-remove-sign "></span>'+"ลบ"+'</a></p></td>'
				+'</tr>');

		  });
  
	  });
});