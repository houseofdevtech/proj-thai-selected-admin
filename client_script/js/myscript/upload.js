$(document).ready(function(){ 
    function validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(str);
      }
    
    $('#upload').change(function(){ 
        var fd = new FormData();
        var files = $('#upload')[0].files[0];
        fd.append('file',files);
        var settings = {
            "url": "api/add_pic.php",
            "type": "post",
            "timeout": 0,
            "data": fd,
            "contentType": false,
            "processData": false,
        };
        $.ajax(settings).done(function(response){
            if(validURL(response)){
                alert("การอัพโหลดรูปเสร็จสมบูรณ์");
                $('#Pic').val(response);
            } else {
                alert("ขออภัย ไม่สามารถอัพโหลดรูปได้\nโปรดตรวจสอบ Error ด้านล่าง:\n"+response);
            }
        });
    }); 
}); 